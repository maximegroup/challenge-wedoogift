package challenge.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import challenge.exception.DistributionException;
import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Distribution;
import challenge.model.User;
import challenge.service.intf.IContentService;
import challenge.service.intf.IDistributionService;
import challenge.service.intf.IJsonService;

class JsonServiceTest {

	private IJsonService jsonService = new JsonService();

	// Test deserializeInput
	@Test
	void deserializeInput_nominal() {
		Content inputContent = jsonService.deserializeInputFile("backend/Level1/data/input.json", Content.class);
		assertThat(inputContent).isNotNull();
		assertThat(inputContent.getCompanies().size()).isEqualTo(2);
		assertThat(inputContent.getUsers().size()).isEqualTo(3);
		assertThat(inputContent.getDistributions().size()).isZero();

		assertThat(inputContent.getCompanies()).hasSize(2)
				.extracting(Company::getId, Company::getName, Company::getBalance)
				.containsExactlyInAnyOrder(tuple(1, "Wedoogift", 1000), tuple(2, "Wedoofood", 3000));

		assertThat(inputContent.getUsers()).hasSize(3).extracting("id", "balance")
				.containsExactlyInAnyOrder(tuple(1, 100), tuple(2, 0), tuple(3, 0));

	}

	@ParameterizedTest
	@MethodSource("provideParametersDeserializeInputNull")
	void deserializeInput_nominal_null(String location, Class<?> clazz) {
		Content inputContent = jsonService.deserializeInputFile(location, clazz);
		assertThat(inputContent).isNotNull();
		assertThat(inputContent.getCompanies()).isEmpty();
		assertThat(inputContent.getUsers()).isEmpty();
		assertThat(inputContent.getDistributions()).isEmpty();
	}

	private static Stream<Arguments> provideParametersDeserializeInputNull() {
		return Stream.of(Arguments.of(null, null), Arguments.of(null, Content.class),
				Arguments.of("backend/Level1/data/input.json", null));
	}

	// Test serializeOutput
	@Test
	void serializeOutput_nominal() {
		Content outputContent = generateOutputContent();
		String outputString = jsonService.serializeOutput(outputContent, false);

		// Right string is different than the expected one because one of the
		// distributions is out of date since 30th of April.
		String stringToTest = "{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":850},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":3000}],\"users\":[{\"id\":1,\"balance\":150},{\"id\":2,\"balance\":100},{\"id\":3,\"balance\":0}],\"distributions\":[{\"id\":1,\"amount\":50,\"start_date\":\"2020-09-16\",\"end_date\":\"2021-09-15\",\"company_id\":1,\"user_id\":1},{\"id\":2,\"amount\":100,\"start_date\":\"2020-08-01\",\"end_date\":\"2021-07-31\",\"company_id\":1,\"user_id\":2},{\"id\":3,\"amount\":1000,\"start_date\":\"2020-05-01\",\"end_date\":\"2021-04-30\",\"company_id\":2,\"user_id\":3}]}";

		assertThat(outputString).isEqualTo(stringToTest);
	}

	@Test
	void serializeOutput_inputNull() {
		String outputString = jsonService.serializeOutput(null, false);
		assertThat(outputString).isEqualTo("null");
	}

	private Content generateOutputContent() {
		IJsonService jsonService = new JsonService();
		Content inputContent = jsonService.deserializeInputFile("backend/Level1/data/input.json", Content.class);

		IContentService contentService = new ContentService();
		Company wedoogift = contentService.getCompanyByName(inputContent, "Wedoogift");
		Company wedoofood = contentService.getCompanyByName(inputContent, "Wedoofood");

		User user1 = contentService.getUserById(inputContent, 1);
		User user2 = contentService.getUserById(inputContent, 2);
		User user3 = contentService.getUserById(inputContent, 3);

		// Creating distribution
		Distribution distribution1 = new Distribution(1, 50, LocalDate.parse("2020-09-16"));
		Distribution distribution2 = new Distribution(2, 100, LocalDate.parse("2020-08-01"));
		Distribution distribution3 = new Distribution(3, 1000, LocalDate.parse("2020-05-01"));

		// Gift card from company to user
		IDistributionService distributionService = new DistributionService();
		try {
			distributionService.distribute(wedoogift, user1, distribution1);
			distributionService.distribute(wedoogift, user2, distribution2);
			distributionService.distribute(wedoofood, user3, distribution3);
		} catch (DistributionException e) {
			e.printStackTrace();
		}
		// Creating ouput
		Content outputContent = new Content();
		outputContent.setCompanies(Arrays.asList(wedoogift, wedoofood));
		outputContent.setUsers(Arrays.asList(user1, user2, user3));
		outputContent.setDistributions(Arrays.asList(distribution1, distribution2, distribution3));
		return outputContent;
	}
}
