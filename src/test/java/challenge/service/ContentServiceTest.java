package challenge.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Content2;
import challenge.model.User;
import challenge.model.User1;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.intf.IContentService;

class ContentServiceTest {

	private IContentService contentService = new ContentService();

	private Content content = null;
	private Company company = null;
	private User user = null;

	private Content2 content2 = null;
	private Wallet wallet = null;

	@BeforeEach
	void createObjects() {
		company = new Company(1, "Wedoogift", 1000);
		List<Company> companies = new ArrayList<>();
		companies.add(company);
		user = new User1(1, 100);
		List<User> users = new ArrayList<>();
		users.add(user);

		content = new Content();
		content.setCompanies(companies);
		content.setUsers(users);

		wallet = new Wallet(1, "food cards", WalletType.FOOD);
		List<Wallet> wallets = new ArrayList<>();
		wallets.add(wallet);

		content2 = new Content2();
		content2.setWallets(wallets);
	}

	@AfterEach
	void cleanObjects() {
		content = null;
		company = null;
		user = null;
		content2 = null;
		wallet = null;
	}

	// Test getCompanyByName
	@Test
	void getCompanyByName_nominal() {
		assertThat(contentService.getCompanyByName(content, "Wedoogift")).isEqualTo(company);
	}

	@Test
	void getCompanyByName_nullContent() {
		assertThat(contentService.getCompanyByName(null, "Wedoogift")).isNull();
	}

	@Test
	void getCompanyByName_nullName() {
		assertThat(contentService.getCompanyByName(content, null)).isNull();
	}

	// Test getCompanyById
	@Test
	void getCompanyById_nominal() {
		assertThat(contentService.getCompanyById(content, 1)).isEqualTo(company);
	}

	@Test
	void getCompanyById_nullContent() {
		assertThat(contentService.getCompanyById(null, 1)).isNull();
	}

	// Test getUserById
	@Test
	void getUserById_nominal() {
		assertThat(contentService.getUserById(content, 1)).isEqualTo(user);
	}

	@Test
	void getUserById_nullContent() {
		assertThat(contentService.getUserById(null, 1)).isNull();
	}

	// Test getWalletByType
	@Test
	void getWalletByType_nominal() {
		assertThat(contentService.getWalletByType(content2, WalletType.FOOD)).isEqualTo(wallet);
	}

	@Test
	void getWalletByType_nullContent() {
		assertThat(contentService.getWalletByType(null, WalletType.FOOD)).isNull();
	}

	@Test
	void getWalletByType_nullWalletType() {
		assertThat(contentService.getWalletByType(content2, null)).isNull();
	}

	// Test getWalletById
	@Test
	void getWalletById_nominal() {
		assertThat(contentService.getWalletById(content2, 1)).isEqualTo(wallet);
	}

	@Test
	void getWalletById_nullContent() {
		assertThat(contentService.getWalletById(null, 1)).isNull();
	}
}
