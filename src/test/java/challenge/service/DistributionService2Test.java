package challenge.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import challenge.exception.DistributionException;
import challenge.model.Balance;
import challenge.model.Company;
import challenge.model.Distribution;
import challenge.model.Distribution2;
import challenge.model.User;
import challenge.model.User2;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.intf.IDistributionService;

class DistributionService2Test {

	private IDistributionService distributionService2 = new DistributionService2();

	private Company company = null;
	private User2 user = null;
	private Balance balance = null;
	private Distribution distributionGift = null;
	private Distribution distributionFood = null;
	private Wallet walletGift = null;
	private Wallet walletFood = null;

	@BeforeEach
	void createObjects() {
		walletGift = new Wallet(1, "gift cards", WalletType.GIFT);
		walletFood = new Wallet(2, "food cards", WalletType.FOOD);

		company = new Company(1, "Wedoogift", 1000);
		balance = new Balance(walletGift, 100);
		user = new User2(1, new ArrayList<>(Arrays.asList(balance)));

		distributionGift = new Distribution2(1, 50, LocalDate.parse("2020-09-16"), walletGift);
		distributionFood = new Distribution2(1, 100, LocalDate.parse("2021-09-16"), walletFood);
	}

	@AfterEach
	void cleanObjects() {
		company = null;
		user = null;
		balance = null;
		distributionGift = null;
		distributionFood = null;
		walletGift = null;
		walletFood = null;
	}

	// Test distribute
	@Test
	void distribute_nominalGift() throws DistributionException {
		distributionService2.distribute(company, user, distributionGift);

		assertThat(distributionGift.getCompany()).isEqualTo(company);
		assertThat(distributionGift.getUser()).isEqualTo(user);
		assertThat(distributionGift.getEndDate()).isEqualTo("2021-09-15");
		assertThat(company.getBalance()).isEqualTo(950);
		assertThat(user.getDistributions()).contains(distributionGift);
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(150, walletGift));
	}

	@Test
	void distribute_nominalFood() throws DistributionException {
		distributionService2.distribute(company, user, distributionFood);

		assertThat(distributionFood.getCompany()).isEqualTo(company);
		assertThat(distributionFood.getUser()).isEqualTo(user);
		assertThat(distributionFood.getEndDate()).isEqualTo("2022-02-28");
		assertThat(company.getBalance()).isEqualTo(900);
		assertThat(user.getDistributions()).contains(distributionFood);
		assertThat(user.getBalances()).hasSize(2).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(100, walletGift), tuple(100, walletFood));
	}

	@Test
	void distribute_companyNotEnoughBalance_error() throws DistributionException {
		company = new Company(1, "Wedoogift", 0);

		assertThatExceptionOfType(DistributionException.class).isThrownBy(() -> {
			distributionService2.distribute(company, user, distributionGift);
		}).withMessageContaining("does not have enough balance.");

		assertThat(distributionGift.getCompany()).isNull();
		assertThat(distributionGift.getUser()).isNull();
		assertThat(company.getBalance()).isZero();
		assertThat(user.getDistributions()).doesNotContain(distributionGift);
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(100, walletGift));
	}

	@Test
	void distribute_expiredDistribution_error() throws DistributionException {
		distributionFood = new Distribution2(1, 100, LocalDate.parse("2020-09-16"), walletFood);

		assertThatExceptionOfType(DistributionException.class).isThrownBy(() -> {
			distributionService2.distribute(company, user, distributionFood);
		}).withMessageContaining("distribution is already expired.");

		assertThat(distributionFood.getCompany()).isEqualTo(company);
		assertThat(distributionFood.getUser()).isEqualTo(user);
		assertThat(company.getBalance()).isEqualTo(1000);
		assertThat(user.getDistributions()).doesNotContain(distributionFood);
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(100, walletGift));
	}

	@ParameterizedTest
	@MethodSource("provideParametersDistributeNull")
	void distribute_null_parameters(Company company, User user, Distribution distribution) {
		try {
			distributionService2.distribute(company, user, distribution);
		} catch (Exception e) {
			fail("Should not have thrown any exception");
		}
		if (distribution != null) {
			assertThat(distribution.getCompany()).isNull();
			assertThat(distribution.getUser()).isNull();
		}
		if (company != null) {
			assertThat(company.getBalance()).isZero();
		}
		if (user != null) {
			assertThat(((User2) user).getBalances()).isEmpty();
			;
			assertThat(user.getDistributions()).isEmpty();
		}
	}

	private static Stream<Arguments> provideParametersDistributeNull() {
		return Stream.of(Arguments.of(null, null, null), Arguments.of(null, null, new Distribution()),
				Arguments.of(null, new User2(), null), Arguments.of(null, new User2(), new Distribution()),
				Arguments.of(new Company(), null, null), Arguments.of(new Company(), null, new Distribution()),
				Arguments.of(new Company(), new User2(), null));
	}

	// Test calculateUserBalance
	@Test
	void calculateUserBalance_nominalGift() {
		distributionGift.setEndDate(LocalDate.parse("2021-09-15"));
		distributionService2.calculateUserBalance(user, Arrays.asList(distributionGift));
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(150, walletGift));
	}

	@Test
	void calculateUserBalance_nominalFood() {
		distributionFood.setEndDate(LocalDate.parse("2022-02-28"));
		distributionService2.calculateUserBalance(user, Arrays.asList(distributionFood));
		assertThat(user.getBalances()).hasSize(2).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(100, walletGift), tuple(100, walletFood));
	}

	@Test
	void calculateUserBalance_expiredDistribution() {
		distributionGift = new Distribution(1, 50, LocalDate.parse("2020-09-16"));
		distributionGift.setEndDate(LocalDate.parse("2021-03-15"));
		distributionService2.calculateUserBalance(user, Arrays.asList(distributionGift));
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(100, walletGift));
	}

	@Test
	void calculateUserBalance_multipleDistribution() {
		distributionGift.setEndDate(LocalDate.parse("2021-09-15"));
		Distribution distribution2 = new Distribution2(1, 100, LocalDate.parse("2020-09-16"), walletGift);
		distribution2.setEndDate(LocalDate.parse("2021-09-15"));
		Distribution distribution3 = new Distribution2(1, 150, LocalDate.parse("2020-09-16"), walletGift);
		distribution3.setEndDate(LocalDate.parse("2021-09-15"));
		distributionService2.calculateUserBalance(user, Arrays.asList(distributionGift, distribution2, distribution3));
		assertThat(user.getBalances()).hasSize(1).extracting(Balance::getAmount, Balance::getWallet)
				.containsExactlyInAnyOrder(tuple(400, walletGift));
	}

	@ParameterizedTest
	@MethodSource("provideParametersCalculateUserBalanceNull")
	void calculateUserBalance_null_parameters(User user, List<Distribution> distributions) {
		distributionService2.calculateUserBalance(user, distributions);
		if (user == null && distributions == null) {
			assertThat(user).isNull();
		} else if (user == null) {
			assertThat(user).isNull();
		} else if (distributions == null) {
			assertThat(((User2) user).getBalances()).isEmpty();
		}
	}

	private static Stream<Arguments> provideParametersCalculateUserBalanceNull() {
		return Stream.of(Arguments.of(null, null), Arguments.of(null, new ArrayList<Distribution>()),
				Arguments.of(new User2(), null));
	}
}
