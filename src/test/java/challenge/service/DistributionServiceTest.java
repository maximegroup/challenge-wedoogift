package challenge.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import challenge.exception.DistributionException;
import challenge.model.Company;
import challenge.model.Distribution;
import challenge.model.User;
import challenge.model.User1;
import challenge.service.intf.IDistributionService;

class DistributionServiceTest {

	private IDistributionService distributionService = new DistributionService();

	private Company company = null;
	private User1 user = null;
	private Distribution distribution = null;

	@BeforeEach
	void createObjects() {
		company = new Company(1, "Wedoogift", 1000);
		user = new User1(1, 100);
		distribution = new Distribution(1, 50, LocalDate.parse("2020-09-16"));
	}

	@AfterEach
	void cleanObjects() {
		company = null;
		user = null;
		distribution = null;
	}

	// Test distribute
	@Test
	void distribute_nominal() throws DistributionException {
		distributionService.distribute(company, user, distribution);

		assertThat(distribution.getCompany()).isEqualTo(company);
		assertThat(distribution.getUser()).isEqualTo(user);
		assertThat(distribution.getEndDate()).isEqualTo("2021-09-15");
		assertThat(company.getBalance()).isEqualTo(950);
		assertThat(user.getDistributions()).contains(distribution);
		assertThat(user.getBalance()).isEqualTo(150);
	}

	@Test
	void distribute_companyNotEnoughBalance_error() throws DistributionException {
		company = new Company(1, "Wedoogift", 0);

		assertThatExceptionOfType(DistributionException.class).isThrownBy(() -> {
			distributionService.distribute(company, user, distribution);
		}).withMessageContaining("does not have enough balance.");

		assertThat(distribution.getCompany()).isNull();
		assertThat(distribution.getUser()).isNull();
		assertThat(distribution.getEndDate()).isNull();
		assertThat(company.getBalance()).isZero();
		assertThat(user.getDistributions()).doesNotContain(distribution);
		assertThat(user.getBalance()).isEqualTo(100);
	}

	@Test
	void distribute_expiredDistribution_error() throws DistributionException {
		distribution = new Distribution(1, 50, LocalDate.parse("2019-09-16"));

		assertThatExceptionOfType(DistributionException.class).isThrownBy(() -> {
			distributionService.distribute(company, user, distribution);
		}).withMessageContaining("distribution is already expired.");

		assertThat(distribution.getCompany()).isEqualTo(company);
		assertThat(distribution.getUser()).isEqualTo(user);
		assertThat(distribution.getEndDate()).isEqualTo("2020-09-14");
		assertThat(company.getBalance()).isEqualTo(1000);
		assertThat(user.getDistributions()).doesNotContain(distribution);
		assertThat(user.getBalance()).isEqualTo(100);
	}

	@ParameterizedTest
	@MethodSource("provideParametersDistributeNull")
	void distribute_null_parameters(Company company, User user, Distribution distribution) {
		try {
			distributionService.distribute(company, user, distribution);
		} catch (Exception e) {
			fail("Should not have thrown any exception");
		}
		if (distribution != null) {
			assertThat(distribution.getCompany()).isNull();
			assertThat(distribution.getUser()).isNull();
			assertThat(distribution.getEndDate()).isNull();
		}
		if (company != null) {
			assertThat(company.getBalance()).isZero();
		}
		if (user != null) {
			assertThat(((User1) user).getBalance()).isZero();
			assertThat(user.getDistributions()).isEmpty();
		}
	}

	private static Stream<Arguments> provideParametersDistributeNull() {
		return Stream.of(Arguments.of(null, null, null), Arguments.of(null, null, new Distribution()),
				Arguments.of(null, new User1(), null), Arguments.of(null, new User1(), new Distribution()),
				Arguments.of(new Company(), null, null), Arguments.of(new Company(), null, new Distribution()),
				Arguments.of(new Company(), new User1(), null));
	}

	// Test calculateUserBalance
	@Test
	void calculateUserBalance_nominal() {
		distribution.setEndDate(LocalDate.parse("2021-09-15"));
		distributionService.calculateUserBalance(user, Arrays.asList(distribution));
		assertThat(user.getBalance()).isEqualTo(150);
	}

	@Test
	void calculateUserBalance_expiredDistribution() {
		distribution = new Distribution(1, 50, LocalDate.parse("2020-09-16"));
		distribution.setEndDate(LocalDate.parse("2021-03-15"));
		distributionService.calculateUserBalance(user, Arrays.asList(distribution));
		assertThat(user.getBalance()).isEqualTo(100);
	}

	@Test
	void calculateUserBalance_multipleDistribution() {
		distribution.setEndDate(LocalDate.parse("2021-09-15"));
		Distribution distribution2 = new Distribution(1, 100, LocalDate.parse("2020-09-16"));
		distribution2.setEndDate(LocalDate.parse("2021-09-15"));
		Distribution distribution3 = new Distribution(1, 150, LocalDate.parse("2020-09-16"));
		distribution3.setEndDate(LocalDate.parse("2021-09-15"));
		distributionService.calculateUserBalance(user, Arrays.asList(distribution, distribution2, distribution3));
		assertThat(user.getBalance()).isEqualTo(400);
	}

	@ParameterizedTest
	@MethodSource("provideParametersCalculateUserBalanceNull")
	void calculateUserBalance_null_parameters(User user, List<Distribution> distributions) {
		distributionService.calculateUserBalance(user, distributions);
		if (user == null && distributions == null) {
			assertThat(user).isNull();
		} else if (user == null) {
			assertThat(user).isNull();
		} else if (distributions == null) {
			assertThat(((User1) user).getBalance()).isZero();
		}
	}

	private static Stream<Arguments> provideParametersCalculateUserBalanceNull() {
		return Stream.of(Arguments.of(null, null), Arguments.of(null, new ArrayList<Distribution>()),
				Arguments.of(new User1(), null));
	}
}
