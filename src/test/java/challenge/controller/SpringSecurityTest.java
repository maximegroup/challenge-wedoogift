
package challenge.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
public class SpringSecurityTest {

	private URL url;
	private TestRestTemplate restTemplate;
	private HttpEntity<MultiValueMap<String, String>> request;

	@BeforeAll
	void createObjects() throws MalformedURLException {
		url = new URL("http://localhost:8080/level1/balance");

		restTemplate = new TestRestTemplate("user", "password");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("contentString",
				"{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":1000},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":3000}],\"users\":[{\"id\":1,\"balance\":100},{\"id\":2,\"balance\":0},{\"id\":3,\"balance\":0}],\"distributions\":[]}");
		map.add("userId", "1");

		request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
	}

	@AfterAll
	void cleanObjects() {
		url = null;
		restTemplate = null;
		request = null;
	}

	@Test
	void rightPassword_nominal() throws IllegalStateException, IOException {
		ResponseEntity<String> response = restTemplate.postForEntity(url.toString(), request, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	void wrongPassword_error() throws Exception {
		restTemplate = new TestRestTemplate("user", "wrongpassword");
		ResponseEntity<String> response = restTemplate.postForEntity(url.toString(), request, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
	}
}
