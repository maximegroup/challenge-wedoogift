package challenge.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Qualifier;

import challenge.exception.DistributionException;
import challenge.model.Balance;
import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Content2;
import challenge.model.Distribution;
import challenge.model.User;
import challenge.model.User1;
import challenge.model.User2;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.intf.IContentService;
import challenge.service.intf.IDistributionService;
import challenge.service.intf.IJsonService;

@ExtendWith(MockitoExtension.class)
class DistributionControllerTest {

	@InjectMocks
	private DistributionController distributionController;

	@Mock
	private IJsonService jsonService;

	@Mock
	private IContentService contentService;

	@Mock
	@Qualifier("distributionService1")
	private IDistributionService distributionService1;

	@Mock
	@Qualifier("distributionService2")
	private IDistributionService distributionService2;

	private String output1 = "{\"companies\":[{\"id\":1,\"name\":\"Wedoogift\",\"balance\":1000},{\"id\":2,\"name\":\"Wedoofood\",\"balance\":3000}],\"users\":[{\"id\":1,\"balance\":100},{\"id\":2,\"balance\":0},{\"id\":3,\"balance\":0}],\"distributions\":[]}";

	private Content content = null;
	private Company company = null;
	private User user1 = null;

	private Content2 content2 = null;
	private User user2 = null;
	private Balance balance = null;
	private Wallet wallet = null;

	@BeforeEach
	void createObjects() throws DistributionException {
		company = new Company(1, "Wedoogift", 1000);
		List<Company> companies = new ArrayList<>();
		companies.add(company);
		user1 = new User1(1, 100);
		List<User> users = new ArrayList<>();
		users.add(user1);

		content = new Content();
		content.setCompanies(companies);
		content.setUsers(users);

		wallet = new Wallet(1, "food cards", WalletType.FOOD);
		List<Wallet> wallets = new ArrayList<>();
		wallets.add(wallet);

		List<Balance> balances = new ArrayList<>();
		balance = new Balance(wallet, 1);
		balances.add(balance);
		user2 = new User2(1, balances);
		List<User> users2 = new ArrayList<>();
		users2.add(user2);

		content2 = new Content2();
		content2.setWallets(wallets);
		content2.setUsers(users2);

		reset(jsonService);
		reset(contentService);
		reset(distributionService1);
		reset(distributionService2);
	}

	@AfterEach
	void cleanObjects() {
		content = null;
		company = null;
		user1 = null;
		content2 = null;
		user2 = null;
		balance = null;
		wallet = null;
	}

	// distributeLevel1
	@Test
	void distributeLevel1_nominal() throws DistributionException {
		when(jsonService.deserializeInputString(any(String.class), any(Class.class))).thenReturn(content);
		when(contentService.getCompanyById(any(Content.class), any(Integer.class))).thenReturn(company);
		when(contentService.getUserById(any(Content.class), any(Integer.class))).thenReturn(user1);
		doNothing().when(distributionService1).distribute(any(Company.class), any(User.class), any(Distribution.class));

		Content contentTest = distributionController.distributeLevel1(output1, 1, 1, 100, "2021-01-01");

		verify(jsonService, times(1)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(1)).getCompanyById(any(Content.class), any(Integer.class));
		verify(contentService, times(1)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService1, times(1)).distribute(any(Company.class), any(User.class), any(Distribution.class));

		assertThat(contentTest.getDistributions()).hasSize(1)
				.extracting(Distribution::getAmount, Distribution::getStartDate)
				.containsExactlyInAnyOrder(tuple(100, LocalDate.parse("2021-01-01")));

	}

	@ParameterizedTest
	@MethodSource("provideParametersDistributeNull")
	void distributeLevel1_nullParameters(String contentString, Integer companyId, Integer userId,
			Integer distributionAmount, String startDate) throws DistributionException {
		Content contentTest = distributionController.distributeLevel1(contentString, companyId, userId,
				distributionAmount, startDate);

		verify(jsonService, times(0)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(0)).getCompanyById(any(Content.class), any(Integer.class));
		verify(contentService, times(0)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService1, times(0)).distribute(any(Company.class), any(User.class), any(Distribution.class));

		assertThat(contentTest.getDistributions()).isEmpty();

	}

	private static Stream<Arguments> provideParametersDistributeNull() {
		return Stream.of(Arguments.of(null, null, null, null, null), Arguments.of("", null, null, null, null));
	}

	// userBalanceLevel1
	@Test
	void userBalanceLevel1_nominal() {
		when(jsonService.deserializeInputString(any(String.class), any(Class.class))).thenReturn(content);
		when(contentService.getUserById(any(Content.class), any(Integer.class))).thenReturn(user1);
		doNothing().when(distributionService1).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		String balanceTest = distributionController.userBalanceLevel1(output1, 1);

		verify(jsonService, times(1)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(1)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService1, times(1)).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		assertThat(balanceTest).isEqualTo("100");

	}

	@ParameterizedTest
	@MethodSource("provideParametersUserBalanceLevel1Null")
	void distributeLevel1_nullParameters(String contentString, Integer userId) throws DistributionException {
		String balanceTest = distributionController.userBalanceLevel1(contentString, userId);

		verify(jsonService, times(0)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(0)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService1, times(0)).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		assertThat(balanceTest).isEqualTo("0");

	}

	private static Stream<Arguments> provideParametersUserBalanceLevel1Null() {
		return Stream.of(Arguments.of(null, null), Arguments.of(null, 1), Arguments.of("", null));
	}

	// distributeLeve2
	@Test
	void distributeLevel2_nominal() throws DistributionException {
		when(jsonService.deserializeInputString(any(String.class), any(Class.class))).thenReturn(content);
		when(contentService.getCompanyById(any(Content.class), any(Integer.class))).thenReturn(company);
		when(contentService.getUserById(any(Content.class), any(Integer.class))).thenReturn(user2);
		doNothing().when(distributionService2).distribute(any(Company.class), any(User.class), any(Distribution.class));

		Content contentTest = distributionController.distributeLevel2(output1, 1, 1, 100, "2021-01-01", 1);

		verify(jsonService, times(1)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(1)).getCompanyById(any(Content.class), any(Integer.class));
		verify(contentService, times(1)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService2, times(1)).distribute(any(Company.class), any(User.class), any(Distribution.class));

		assertThat(contentTest.getDistributions()).hasSize(1)
				.extracting(Distribution::getAmount, Distribution::getStartDate)
				.containsExactlyInAnyOrder(tuple(100, LocalDate.parse("2021-01-01")));

	}

	@ParameterizedTest
	@MethodSource("provideParametersDistribute2Null")
	void distributeLevel2_nullParameters(String contentString, Integer companyId, Integer userId,
			Integer distributionAmount, String startDate, Integer walletId) throws DistributionException {
		Content contentTest = distributionController.distributeLevel2(contentString, companyId, userId,
				distributionAmount, startDate, walletId);

		verify(jsonService, times(0)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(0)).getCompanyById(any(Content.class), any(Integer.class));
		verify(contentService, times(0)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService2, times(0)).distribute(any(Company.class), any(User.class), any(Distribution.class));

		assertThat(contentTest.getDistributions()).isEmpty();

	}

	private static Stream<Arguments> provideParametersDistribute2Null() {
		return Stream.of(Arguments.of(null, null, null, null, null, null),
				Arguments.of("", null, null, null, null, null));
	}

	// userBalanceLevel1
	@Test
	void userBalanceLevel2_nominal() {
		when(jsonService.deserializeInputString(any(String.class), any(Class.class))).thenReturn(content);
		when(contentService.getUserById(any(Content.class), any(Integer.class))).thenReturn(user2);
		doNothing().when(distributionService2).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		String balanceTest = distributionController.userBalanceLevel2(output1, 1);

		verify(jsonService, times(1)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(1)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService2, times(1)).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		assertThat(balanceTest).isEqualTo(((User2) user2).getBalances().toString());

	}

	@ParameterizedTest
	@MethodSource("provideParametersUserBalanceLeve21Null")
	void distributeLevel2_nullParameters(String contentString, Integer userId) throws DistributionException {
		String balanceTest = distributionController.userBalanceLevel2(contentString, userId);

		verify(jsonService, times(0)).deserializeInputString(any(String.class), any(Class.class));
		verify(contentService, times(0)).getUserById(any(Content.class), any(Integer.class));
		verify(distributionService2, times(0)).calculateUserBalance(any(User.class), ArgumentMatchers.isNull());

		assertThat(balanceTest).isEqualTo("");

	}

	private static Stream<Arguments> provideParametersUserBalanceLeve21Null() {
		return Stream.of(Arguments.of(null, null), Arguments.of(null, 1), Arguments.of("", null));
	}
}
