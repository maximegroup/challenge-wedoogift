package challenge.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Content2;
import challenge.model.Distribution;
import challenge.model.Distribution2;
import challenge.model.User1;
import challenge.model.User2;
import challenge.model.Wallet;
import challenge.service.intf.IContentService;
import challenge.service.intf.IDistributionService;
import challenge.service.intf.IJsonService;

@RestController
public class DistributionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributionController.class);

	@Autowired
	private IJsonService jsonService;

	@Autowired
	private IContentService contentService;

	@Autowired
	@Qualifier("distributionService1")
	private IDistributionService distributionService1;

	@Autowired
	@Qualifier("distributionService2")
	private IDistributionService distributionService2;

	private int idDistribution = 1;

	@PostMapping(value = "/level1/distribute")
	@ResponseBody
	public Content distributeLevel1(@RequestParam String contentString, @RequestParam Integer companyId,
			@RequestParam Integer userId, @RequestParam Integer distributionAmount, @RequestParam String startDate) {
		Content content = new Content();
		try {
			if (contentString != null && companyId != null && userId != null && distributionAmount != null) {
				content = jsonService.deserializeInputString(contentString, Content.class);
				LOGGER.info("Input content: {}", content);
				Company company = contentService.getCompanyById(content, companyId);
				User1 user = (User1) contentService.getUserById(content, userId);
				Distribution distribution = new Distribution(idDistribution++, distributionAmount,
						LocalDate.parse(startDate));
				distributionService1.distribute(company, user, distribution);
				if (content.getDistributions() == null) {
					content.setDistributions(new ArrayList<>());
				}
				content.getDistributions().add(distribution);
				LOGGER.info("Output content: {}", content);
			}
		} catch (Exception e) {
			LOGGER.error("distributeLevel1 failed", e);
		}
		return content;

	}

	@PostMapping(value = "/level1/balance")
	@ResponseBody
	public String userBalanceLevel1(@RequestParam String contentString, @RequestParam Integer userId) {
		String userBalance = "0";
		try {
			if (contentString != null && userId != null) {
				Content content = jsonService.deserializeInputString(contentString, Content.class);
				LOGGER.info("Input content: {}", content);
				User1 user = (User1) contentService.getUserById(content, userId);
				distributionService1.calculateUserBalance(user, null);
				userBalance = String.valueOf(user.getBalance());
				LOGGER.info("User balance: {}", userBalance);
			}
		} catch (Exception e) {
			LOGGER.error("userBalanceLevel1 failed", e);
		}
		return userBalance;

	}

	@PostMapping(value = "/level2/distribute")
	@ResponseBody
	public Content distributeLevel2(@RequestParam String contentString, @RequestParam Integer companyId,
			@RequestParam Integer userId, @RequestParam Integer distributionAmount, @RequestParam String startDate,
			@RequestParam Integer walletId) {
		Content content = new Content();
		try {
			if (contentString != null && companyId != null && userId != null && distributionAmount != null) {
				content = jsonService.deserializeInputString(contentString, Content2.class);
				LOGGER.info("Input content: {}", content);
				Company company = contentService.getCompanyById(content, companyId);
				User2 user = (User2) contentService.getUserById(content, userId);
				Wallet wallet = contentService.getWalletById(content, walletId);
				Distribution2 distribution = new Distribution2(idDistribution++, distributionAmount,
						LocalDate.parse(startDate), wallet);
				distributionService2.distribute(company, user, distribution);
				if (content.getDistributions() == null) {
					content.setDistributions(new ArrayList<>());
				}
				content.getDistributions().add(distribution);
				LOGGER.info("Output content: {}", content);
			}
		} catch (Exception e) {
			LOGGER.error("distributeLevel2 failed", e);
		}
		return content;

	}

	@PostMapping(value = "/level2/balance")
	@ResponseBody
	public String userBalanceLevel2(@RequestParam String contentString, @RequestParam Integer userId) {
		String userBalance = "";
		try {
			if (contentString != null && userId != null) {
				Content content = jsonService.deserializeInputString(contentString, Content2.class);
				LOGGER.info("Input content: {}", content);
				User2 user = (User2) contentService.getUserById(content, userId);
				distributionService2.calculateUserBalance(user, null);
				userBalance = String.valueOf(user.getBalances());
				LOGGER.info("User balance: {}", userBalance);
			}
		} catch (Exception e) {
			LOGGER.error("userBalanceLevel1 failed", e);
		}
		return userBalance;
	}
}
