package challenge.model;

public class User1 extends User {

	private int balance;

	public User1() {
		super();
	}

	public int getBalance() {
		return balance;
	}

	public User1(int id, int balance) {
		super(id);
		this.balance = balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "User1 [balance=" + balance + ", id=" + id + "]";
	}
}
