package challenge.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonPropertyOrder({ "id", "wallet", "amount", "startDate", "endDate", "company", "user" })
public class Distribution2 extends Distribution {

	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonIdentityReference(alwaysAsId = true)
	@JsonProperty("wallet_id")
	private Wallet wallet;

	public Distribution2() {
		super();
	}

	public Distribution2(int id, int amount, LocalDate startDate, Wallet wallet) {
		super(id, amount, startDate);
		this.wallet = wallet;
	}

	public Distribution2(Wallet wallet) {
		super();
		this.wallet = wallet;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	@Override
	public String toString() {
		return "Distribution2 [wallet=" + (wallet != null ? wallet.getId() : "") + ", id=" + id + ", amount=" + amount
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", company="
				+ (company != null ? company.getId() : "") + ", user=" + (user != null ? user.getId() : "") + "]";
	}
}
