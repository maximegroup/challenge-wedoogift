package challenge.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.DEDUCTION)
@JsonSubTypes({ @Type(value = User1.class, name = "user1"), @Type(value = User2.class, name = "user2") })
public class User {

	protected int id;

	@JsonIgnore
	protected List<Distribution> distributions = new ArrayList<>();

	public User() {
	}

	public User(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Distribution> getDistributions() {
		return distributions;
	}

	public void addDistribution(Distribution distribution) {
		distributions.add(distribution);
	}

	public void removeDistribution(Distribution distribution) {
		distributions.remove(distribution);
	}

	@Override
	public String toString() {
		return "User [id=" + id + "]";
	}

}
