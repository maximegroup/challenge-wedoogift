package challenge.model;

import java.util.ArrayList;
import java.util.List;

public class User2 extends User {

	// Had to rename balance in input to balances to make it work with Jackson.
	// Maybe there is another solution.
	private List<Balance> balances = new ArrayList<>();

	public User2() {
		super();
	}

	public User2(int id, List<Balance> balance) {
		super(id);
		this.balances = balance;
	}

	public List<Balance> getBalances() {
		return balances;
	}

	public void setBalances(List<Balance> balance) {
		this.balances = balance;
	}

	@Override
	public String toString() {
		return "User2 [balances=" + balances + ", id=" + id + "]";
	}

}
