package challenge.model;

import java.util.ArrayList;
import java.util.List;

public class Content {

	protected List<Company> companies = new ArrayList<>();

	protected List<User> users = new ArrayList<>();

	protected List<Distribution> distributions = new ArrayList<>();

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Distribution> getDistributions() {
		return distributions;
	}

	public void setDistributions(List<Distribution> distributions) {
		this.distributions = distributions;
	}

	@Override
	public String toString() {
		return "Content [companies=" + companies + ", users=" + users + ", distributions=" + distributions + "]";
	}
}
