package challenge.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "wallet", "amount" })
public class Balance {

	@JsonIdentityReference(alwaysAsId = true)
	@JsonProperty("wallet_id")
	private Wallet wallet;

	private int amount;

	public Balance() {
	}

	public Balance(Wallet wallet, int amount) {
		this.wallet = wallet;
		this.amount = amount;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Balance [wallet=" + (wallet != null ? String.valueOf(wallet.getId()) : -1) + ", amount=" + amount + "]";
	}

}
