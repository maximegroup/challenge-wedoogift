package challenge.model;

import java.util.ArrayList;
import java.util.List;

public class Content2 extends Content {

	private List<Wallet> wallets = new ArrayList<>();;

	public List<Wallet> getWallets() {
		return wallets;
	}

	public void setWallets(List<Wallet> wallets) {
		this.wallets = wallets;
	}

	@Override
	public String toString() {
		return "Content2 [wallets=" + wallets + ", companies=" + companies + ", users=" + users + ", distributions="
				+ distributions + "]";
	}
}
