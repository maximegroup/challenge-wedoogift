package challenge.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(scope = Wallet.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Wallet {

	private int id;

	private String name;

	private WalletType type;

	public Wallet() {
	}

	public Wallet(int id, String name, WalletType type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WalletType getType() {
		return type;
	}

	public void setType(WalletType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Wallet [id=" + id + ", name=" + name + ", type=" + type + "]";
	}

}
