package challenge.exception;

public class DistributionException extends Exception {

	private static final long serialVersionUID = -1133348034103229552L;

	public DistributionException(String errorMessage) {
		super(errorMessage);
	}
}
