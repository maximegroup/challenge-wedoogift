package challenge;

import java.time.LocalDate;
import java.util.Arrays;

import challenge.exception.DistributionException;
import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Content2;
import challenge.model.Distribution2;
import challenge.model.User;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.ContentService;
import challenge.service.DistributionService2;
import challenge.service.JsonService;
import challenge.service.intf.IContentService;
import challenge.service.intf.IDistributionService;
import challenge.service.intf.IJsonService;

public class MainLevel2 {

	/**
	 * Main program showing how the use case of the level 2.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Read input from Json file
		IJsonService jsonService = new JsonService();
		Content inputContent = jsonService.deserializeInputFile("backend/Level2/data/input.json", Content2.class);

		IContentService contentService = new ContentService();
		Company wedoogift = contentService.getCompanyByName(inputContent, "Wedoogift");
		Company wedoofood = contentService.getCompanyByName(inputContent, "Wedoofood");

		User user1 = contentService.getUserById(inputContent, 1);
		User user2 = contentService.getUserById(inputContent, 2);
		User user3 = contentService.getUserById(inputContent, 3);

		Wallet wallet1 = contentService.getWalletByType(inputContent, WalletType.GIFT);
		Wallet wallet2 = contentService.getWalletByType(inputContent, WalletType.FOOD);

		// Creating distribution
		Distribution2 distribution1 = new Distribution2(1, 50, LocalDate.parse("2020-09-16"), wallet1);
		Distribution2 distribution2 = new Distribution2(2, 100, LocalDate.parse("2020-08-01"), wallet1);
		Distribution2 distribution3 = new Distribution2(3, 1000, LocalDate.parse("2020-05-01"), wallet1);
		Distribution2 distribution4 = new Distribution2(4, 250, LocalDate.parse("2020-05-01"), wallet2);

		// Gift card from company to user
		IDistributionService distributionService = new DistributionService2();
		try {
			distributionService.distribute(wedoogift, user1, distribution1);
		} catch (DistributionException e) {
			e.printStackTrace();
		}

		try {
			distributionService.distribute(wedoogift, user2, distribution2);
		} catch (DistributionException e) {
			e.printStackTrace();
		}

		try {
			distributionService.distribute(wedoofood, user3, distribution3);
		} catch (DistributionException e) {
			e.printStackTrace();
		}

		try {
			distributionService.distribute(wedoogift, user1, distribution4);
		} catch (DistributionException e) {
			e.printStackTrace();
		}

		// Creating ouput
		Content outputContent = new Content();
		outputContent.setCompanies(Arrays.asList(wedoogift, wedoofood));
		outputContent.setUsers(Arrays.asList(user1, user2, user3));
		outputContent.setDistributions(Arrays.asList(distribution1, distribution2, distribution3, distribution4));

		String output = jsonService.serializeOutput(outputContent, true);
		System.out.println(output);
	}
}
