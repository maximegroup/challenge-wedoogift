package challenge.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import challenge.exception.DistributionException;
import challenge.model.Company;
import challenge.model.Distribution;
import challenge.model.User;
import challenge.model.User1;
import challenge.service.intf.IDistributionService;

@Service("distributionService1")
public class DistributionService implements IDistributionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributionService.class);

	public static final int GIFT_CARD_LIFESPAN = 364;

	@Override
	public void distribute(Company company, User user, Distribution distribution) throws DistributionException {
		if (company != null && user != null && distribution != null && distribution.getStartDate() != null) {
			if (company.getBalance() < distribution.getAmount()) {
				throw new DistributionException(company.getName() + " does not have enough balance.");
			}
			distribution.setEndDate(distribution.getStartDate().plusDays(GIFT_CARD_LIFESPAN));
			distribution.setCompany(company);
			distribution.setUser(user);
			if (LocalDate.now().isAfter(distribution.getEndDate())) {
				throw new DistributionException(distribution.getId() + " distribution is already expired.");
			}
			company.setBalance(company.getBalance() - distribution.getAmount());
			user.addDistribution(distribution);
			calculateUserBalance(user, Arrays.asList(distribution));
		} else {
			LOGGER.error("distribute did not have the right arguments.");
		}
	}

	@Override
	public void calculateUserBalance(User user, List<Distribution> distributions) {
		int balance = 0;
		if (user instanceof User1 && distributions != null) {
			User1 user1 = ((User1) user);
			balance = user1.getBalance();
			balance += distributions.stream().filter(distribution -> distribution.getEndDate().isAfter(LocalDate.now()))
					.mapToDouble(Distribution::getAmount).sum();
			user1.setBalance(balance);
		} else {
			LOGGER.error("calculateUserBalance did not have the right arguments.");
		}
	}

}
