package challenge.service;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import challenge.model.Company;
import challenge.model.Content;
import challenge.model.Content2;
import challenge.model.User;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.intf.IContentService;

@Service
public class ContentService implements IContentService {

	@Override
	public Company getCompanyByName(Content content, String name) {
		Company company = null;
		if (content != null && content.getCompanies() != null && name != null) {
			Optional<Company> optionalCompany = content.getCompanies().stream().filter(c -> name.equals(c.getName()))
					.limit(1).collect(Collectors.reducing((a, b) -> null));
			if (optionalCompany.isPresent()) {
				company = optionalCompany.get();
			}
		}
		return company;
	}

	@Override
	public Company getCompanyById(Content content, int id) {
		Company company = null;
		if (content != null && content.getCompanies() != null) {
			Optional<Company> optionalCompany = content.getCompanies().stream().filter(c -> id == c.getId()).limit(1)
					.collect(Collectors.reducing((a, b) -> null));
			if (optionalCompany.isPresent()) {
				company = optionalCompany.get();
			}
		}
		return company;
	}

	@Override
	public User getUserById(Content content, int id) {
		User user = null;
		if (content != null && content.getUsers() != null) {
			Optional<User> optionalCompany = content.getUsers().stream().filter(u -> id == u.getId()).limit(1)
					.collect(Collectors.reducing((a, b) -> null));
			if (optionalCompany.isPresent()) {
				user = optionalCompany.get();
			}
		}
		return user;
	}

	@Override
	public Wallet getWalletByType(Content content, WalletType walletType) {
		Wallet wallet = null;
		if (content instanceof Content2 && ((Content2) content).getWallets() != null && walletType != null) {
			Content2 content2 = (Content2) content;
			Optional<Wallet> optionalCompany = content2.getWallets().stream()
					.filter(w -> walletType.equals(w.getType())).limit(1).collect(Collectors.reducing((a, b) -> null));
			if (optionalCompany.isPresent()) {
				wallet = optionalCompany.get();
			}
		}
		return wallet;
	}

	@Override
	public Wallet getWalletById(Content content, int id) {
		Wallet wallet = null;
		if (content instanceof Content2 && ((Content2) content).getWallets() != null) {
			Content2 content2 = (Content2) content;
			Optional<Wallet> optionalCompany = content2.getWallets().stream().filter(w -> id == w.getId()).limit(1)
					.collect(Collectors.reducing((a, b) -> null));
			if (optionalCompany.isPresent()) {
				wallet = optionalCompany.get();
			}
		}
		return wallet;
	}

}
