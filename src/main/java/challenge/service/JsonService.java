package challenge.service;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import challenge.model.Content;
import challenge.service.intf.IJsonService;

@Service
public class JsonService implements IJsonService {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonService.class);

	private ObjectMapper objectMapper = null;

	@Override
	public Content deserializeInputFile(String inputLocation, Class<?> clazz) {
		createObjectMapper();
		Content inputContent = new Content();
		try {
			inputContent = objectMapper.readerFor(clazz).readValue(new File(inputLocation));
		} catch (Exception e) {
			LOGGER.error("deserializeInputFile failed", e);
		}
		return inputContent;
	}

	@Override
	public Content deserializeInputString(String input, Class<?> clazz) {
		createObjectMapper();
		Content inputContent = new Content();
		try {
			inputContent = objectMapper.readerFor(clazz).readValue(input);
		} catch (Exception e) {
			LOGGER.error("deserializeInputString failed", e);
		}
		return inputContent;
	}

	@Override
	public String serializeOutput(Content content, boolean prettyPrint) {
		createObjectMapper();
		String outputString = "";
		try {
			if (prettyPrint) {
				outputString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(content);
			} else {
				outputString = objectMapper.writeValueAsString(content);
			}
		} catch (Exception e) {
			LOGGER.error("serializeOutput failed", e);
		}
		return outputString;
	}

	private void createObjectMapper() {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			objectMapper.registerModule(new JavaTimeModule());
		}
	}

}
