package challenge.service.intf;

import challenge.model.Company;
import challenge.model.Content;
import challenge.model.User;
import challenge.model.Wallet;
import challenge.model.WalletType;

public interface IContentService {

	/**
	 * Utility method to get company by name in content object
	 * 
	 * @param content object containing company
	 * @param name    name of the company
	 * @return company from content
	 */
	Company getCompanyByName(Content content, String name);

	/**
	 * Utility method to get company by id in content object
	 * 
	 * @param content object containing company
	 * @param id      id of the company
	 * @return company from content
	 */
	Company getCompanyById(Content content, int id);

	/**
	 * Utility method to get user by id in content object
	 * 
	 * @param content object containing user
	 * @param id      id of the user
	 * @return user from content
	 */
	User getUserById(Content content, int id);

	/**
	 * Utility method to get wallet from content object
	 * 
	 * @param content    object containing user
	 * @param walletType type of wallet
	 * @return wallet from content
	 */
	Wallet getWalletByType(Content content, WalletType walletType);

	/**
	 * 
	 * @param content object containing user
	 * @param id      id of the wallet
	 * @return wallet from content
	 */
	Wallet getWalletById(Content content, int id);

}
