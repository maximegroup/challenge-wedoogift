package challenge.service.intf;

import challenge.model.Content;

public interface IJsonService {

	/**
	 * Deserialize input file into Content object
	 * 
	 * @param inputLocation location of the input file
	 * @param clazz         Class to deserialize to
	 * @return Content object with all data
	 */
	Content deserializeInputFile(String inputLocation, Class<?> clazz);

	/**
	 * Deserialize input string into Content object
	 * 
	 * @param input json input
	 * @param clazz class to deserialize to
	 * @return Content object with all data
	 */
	Content deserializeInputString(String input, Class<?> clazz);

	/**
	 * Serialize content object into Json string with option for pretty print
	 * 
	 * @param content     object with all data
	 * @param prettyPrint boolean for pretty print
	 * @return Json string
	 */
	String serializeOutput(Content content, boolean prettyPrint);

}
