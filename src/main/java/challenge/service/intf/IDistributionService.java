package challenge.service.intf;

import java.util.List;

import challenge.exception.DistributionException;
import challenge.model.Company;
import challenge.model.Distribution;
import challenge.model.User;

public interface IDistributionService {

	/**
	 * Methode distributing distribution from a company to a user if the company has
	 * enough balance.
	 * 
	 * @param company      the company giving distribution
	 * @param user         the user receiving the distribution
	 * @param distribution the distribution being given to a user
	 * @throws DistributionException when bank balance is too low for the
	 *                               distribution
	 */
	void distribute(Company company, User user, Distribution distribution) throws DistributionException;

	/**
	 * Calculate user balance by summing the current user balance and the list of
	 * distributions in parameter and setting it into the user.
	 * 
	 * @param user          a user receiving distributions
	 * @param distributions distributions being added to the user
	 */
	void calculateUserBalance(User user, List<Distribution> distributions);
}
