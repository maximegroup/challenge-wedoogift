package challenge.service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import challenge.exception.DistributionException;
import challenge.model.Balance;
import challenge.model.Company;
import challenge.model.Distribution;
import challenge.model.Distribution2;
import challenge.model.User;
import challenge.model.User2;
import challenge.model.Wallet;
import challenge.model.WalletType;
import challenge.service.intf.IDistributionService;

@Service("distributionService2")
public class DistributionService2 implements IDistributionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DistributionService2.class);

	public static final int GIFT_CARD_LIFESPAN = 364;

	@Override
	public void distribute(Company company, User user, Distribution distribution) throws DistributionException {
		if (company != null && user instanceof User2 && distribution instanceof Distribution2
				&& distribution.getStartDate() != null && ((Distribution2) distribution).getWallet() != null) {
			if (company.getBalance() < distribution.getAmount()) {
				throw new DistributionException(company.getName() + " does not have enough balance.");
			}
			if (WalletType.FOOD.equals(((Distribution2) distribution).getWallet().getType())) {
				distribution.setEndDate(getNextMealVoucherExpiration(distribution.getStartDate()));
			} else if (WalletType.GIFT.equals(((Distribution2) distribution).getWallet().getType())) {
				distribution.setEndDate(distribution.getStartDate().plusDays(GIFT_CARD_LIFESPAN));
			}
			distribution.setCompany(company);
			distribution.setUser(user);
			if (LocalDate.now().isAfter(distribution.getEndDate())) {
				throw new DistributionException(distribution.getId() + " distribution is already expired.");
			}
			company.setBalance(company.getBalance() - distribution.getAmount());
			user.addDistribution(distribution);
			calculateUserBalance(user, Arrays.asList(distribution));
		} else {
			LOGGER.error("distribute did not have the right arguments.");
		}
	}

	private LocalDate getNextMealVoucherExpiration(LocalDate startDate) {
		int startDateYear = startDate.getYear();
		LocalDate expirationDate = LocalDate.of(startDateYear, Month.FEBRUARY, 28);
		if (LocalDate.now().isAfter(expirationDate)) {
			expirationDate = expirationDate.plusYears(1);
		}
		return expirationDate;
	}

	@Override
	public void calculateUserBalance(User user, List<Distribution> distributions) {
		List<Balance> balances = new ArrayList<>();
		if (user instanceof User2 && distributions != null) {
			User2 user2 = (User2) user;
			if (user2.getBalances() != null) {
				balances = user2.getBalances();
			}

			for (Distribution distribution : distributions) {
				if (distribution instanceof Distribution2) {
					Wallet wallet = ((Distribution2) distribution).getWallet();
					if (wallet != null) {
						processBalance(balances, distribution, wallet);
					} else {
						LOGGER.error("Wallet was null.");
					}
				} else {
					LOGGER.error("Wrong type for distribution.");
				}
			}
		} else {
			LOGGER.error("calculateUserBalance did not have the right arguments.");
		}
	}

	private void processBalance(List<Balance> balances, Distribution distribution, Wallet wallet) {
		Balance balance = getBalanceByWalletId(balances, wallet.getId());
		if (balance == null) {
			balance = new Balance();
			balance.setWallet(wallet);
			balances.add(balance);
		}
		if (LocalDate.now().isBefore(distribution.getEndDate())) {
			balance.setAmount(balance.getAmount() + distribution.getAmount());
		} else {
			// Log an error
		}
	}

	private Balance getBalanceByWalletId(List<Balance> balances, int walletId) {
		return balances.stream()
				.filter(balance -> balance.getWallet() != null && walletId == balance.getWallet().getId()).findAny()
				.orElse(null);
	}

}
